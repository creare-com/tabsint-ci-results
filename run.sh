#!/bin/bash -ex

echo Top Level Directory Listing:
pwd
dir
echo tabsint-ci-results Directory Listing:
(cd tabsint-ci-results; pwd; dir)
mkdir -p public
cp tabsint-ci-results/index.html public/index.html
cp tabsint-ci-results/script.js public/script.js
cp tabsint-ci-results/style.css public/style.css
echo public Directory Listing:
(cd public; pwd; dir)
echo python Version:
conda info
pip install ijson
echo Conda Package List:
conda list
python tabsint-ci-results/generate_results.py
echo public Directory Listing:
(cd public; pwd; dir)
