import json
import glob
import shutil
import os
from pprint import pprint

# set up destination folder for copying result json files
destination = "public/results"
if not os.path.exists(destination):
    os.mkdir(destination)

# Get all result folders
folders = glob.glob('*/')
# Remove the public directory - no results there!
folders.remove('public/')

print('Current Working Directory: {}'.format(os.getcwd()))
print('Result Directories To Process: {}'.format(folders))

# initialize list to store sortable values for all results
dataArray = []

# standard fields to grab from top level
headerFields = ['siteId', 'protocolId', 'protocolName', 'testDateTime', 'elapsedTime', 'nResponses']
# standard fields to grab from testResults level
headerFieldsFromTestResults = ['platform', 'tabletUUID', 'tabletModel', 'network']
    
# Iterate through result folders (one for each protocol)
for folder in folders:
    subDestination = os.path.join(destination, folder)
    print('folder: {}, subDestination: {}'.format(folder, subDestination))
    if not os.path.exists(subDestination):
        os.mkdir(subDestination)
    files = glob.glob(os.path.join(folder,'*.json'))
    
    # iterate through result files in this folder/protocol
    for filePath in files:
        newFilePath = shutil.copy(filePath, subDestination) # copy and only open/operate on the copy
        print('newFilePath: {}'.format(newFilePath))
        with open(newFilePath,'r') as f:
            data = json.load(f)
            header = {}
            header['filename'] = newFilePath.replace('public/','')
            for key in headerFields:
                header[key] = data[key] if key in data else None
            for key in headerFieldsFromTestResults:
                if 'testResults' in data:
                    header[key] = data['testResults'][key] if key in data['testResults'] else None
                else:
                    header[key] = None
            dataArray.append(header)
            
# Save the final data to a json file
with open('public/data.json', 'w') as outfile:
    json.dump(dataArray, outfile)
    