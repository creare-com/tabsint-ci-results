// Code goes here

var app = angular.module('myApp', ['rzModule', 'ui.bootstrap', 'jsonFormatter']);
  app.controller('myCtrl', function($scope, $q) {
      window.ss = $scope;

      $scope.dataHeader = undefined;
      $scope.data = {
          header: undefined,
          categories: [],
      	  protocolNames: [],
      	  tabletUUIDs: [],
          nResponses: [],
          searchResults: undefined,
          loadedData: undefined,
          presentationIds: [],
          formattedData: {
            byTest: undefined,
            byResponse: undefined,
            matches: undefined
          }
      };
      $scope.userInput = {
          category: undefined,
          startDate: new Date(2017, 1, 1, 1, 1), //new Date(),
          endDate: new Date(),
          protocolName: 'uconn_sound_rec2',
          protocolNames: {},
          tabletUUID: undefined,
          tabletUUIDs: {},
          nResponses: {},
          nResponsesMin: undefined,
          nResponsesMax: undefined,
          headerFields: {},
          presentationId: undefined
      };
      $scope.clearStartDate = function() {$scope.userInput.startDate = undefined; $scope.onDateChange(); };
      $scope.clearEndDate = function() {$scope.userInput.endDate = undefined; $scope.onDateChange(); };
      $scope.clearProtocolName = function() {$scope.userInput.protocolName = undefined; $scope.onProtocolNameChange(); };
      $scope.clearAllProtocolNames = function() {$scope.userInput.protocolNames = {}; $scope.onProtocolNameChange(); };
      $scope.selectAllProtocolNames = function() { $scope.data.protocolNames.forEach(function(val) {$scope.userInput.protocolNames[val] = true;});  $scope.onProtocolNameChange(); };
      $scope.clearTabletUUID = function() {$scope.userInput.tabletUUID = undefined; $scope.onTabletUUIDChange(); };
      $scope.clearAllTabletUUIDs = function() {$scope.userInput.tabletUUIDs = {}; $scope.onTabletUUIDChange(); };
      $scope.selectAllTabletUUIDs = function() { $scope.data.tabletUUIDs.forEach(function(val) {$scope.userInput.tabletUUIDs[val] = true;});  $scope.onTabletUUIDChange(); };
      $scope.clearAllNResponses = function() {$scope.userInput.nResponses = {}; $scope.onNResponsesChange(); };
      $scope.selectAllNResponses = function() { $scope.data.nResponses.forEach(function(val) {$scope.userInput.nResponses[val] = true;});  $scope.onNResponsesChange(); };
      $scope.onNResponsesRangeChange = function() { console.log('nResponsesSlider: ' + $scope.options.selectRangeNResponses); $scope.onNResponsesChange(); };
      $scope.onDateChange = function() {
          $scope.search();
      };
      $scope.onProtocolNameChange = function() {
          $scope.search();
      };
      $scope.onTabletUUIDChange = function() {
          $scope.search();
      };
      $scope.onNResponsesChange = function() {
          $scope.search();
      };
      $scope.options = {
      	selectMultipleProtocols: false,
      	selectMultipleTabletUUIDs: false,
        selectRangeNResponses: true,
        nResponsesSlider: {floor:0,ceil:0,step:1, onChange:$scope.onNResponsesChange},
        showDetailedResults: true
      };
      
      var formatter = {
        yesNoResponseArea: {},
        multipleChoiceResponseArea: {}
      };
      
      formatter.yesNoResponseArea = {
        setup: function(res) {
          parsedArray = [];
          
          return parsedArray
        }
      };
      
      // format a specific selected presentation id
      $scope.onSelectedPresentationIdChange = function() {
        if (angular.isDefined($scope.userInput.presentationId)) {
          fields = ['protocolName','localTestDateTime','presentationId','response','correct']; // set fields based on presentationId
          $scope.data.formattedData.fields = fields;
          $scope.data.formattedData.matchingResponses = [];
          $scope.data.formattedData.byTest = [];
          $scope.data.formattedData.byResponse = {fields:fields,data:[]};
          $scope.data.loadedData.forEach(function(test) {
            var matches = test.testResults.responses.filter(function(response) {
              return response.presentationId === $scope.userInput.presentationId;
            });
            
            // add any desired test-level details to each response
            matches.forEach(function(match) {
              match.localTestDateTime = test.localTestDateTime;
              match.protocolName = test.protocolName;
            })
            
            $scope.data.formattedData.matchingResponses = $scope.data.formattedData.matchingResponses.concat(matches);
            // push this match array onto the array of tests
            $scope.data.formattedData.byTest.push(matches);
          });
          $scope.data.formattedData.matchingResponses.forEach(function(response) {
            var ret = {}
            fields.forEach(function(field){
              ret[field] = response[field];
            });
            $scope.data.formattedData.byResponse.data.push(ret);
          });
          console.log(JSON.stringify($scope.data.formattedData.byTest));
          $scope.data.formattedData.byTest.forEach(function(test, testInd, testArr){
            var testData = [];
            test.forEach(function(response) {
              var ret = {}
              fields.forEach(function(field){
                ret[field] = response[field];
              });
              testData.push(ret);
            });
            testArr[testInd] = testData;
          });
          console.log(JSON.stringify($scope.data.formattedData.byTest));
        }
      };
      
      // Load the matches from searchResults ($scope.data.searchResult)
      $scope.load = function() {
          var dataArray = [];
          var promise = $q.when();
          $scope.data.searchResults.forEach(function(res) {
              promise = promise.then(function(){
                  console.log('loading: ' + res.filename);
                  return $.ajax({
                      url : res.filename,
                      dataType: "json",
                      success : function(dataFromFile){
                        var d = new Date(res.testDateTime);
                        dataFromFile.localTestDateTime =  d.toLocaleString();
                        dataArray.push(dataFromFile)
                      }
                  });
              });
          });
          
          promise.then(function(){
              $scope.data.loadedData = dataArray;
              var presentations = [];
              dataArray.forEach(function(exam){
                  if (exam && exam.testResults && exam.testResults.responses) {
                      exam.testResults.responses.forEach(function(response) {
                          if (presentations.indexOf(response.presentationId) < 0) {
                              presentations.push(response.presentationId);
                          }
                      });
                  }
              });
              $scope.data.presentationIds = presentations;
          })
      };
      
      // find protocols matching ALL established criteria
  	$scope.search = function() {
          searchResults = $scope.data.header;
          
          // Start Date
          if (angular.isDefined($scope.userInput.startDate)) {
		        	searchResults = searchResults.filter(function(res){ 
                	var testDateTime = new Date(res.testDateTime);
                  return testDateTime >= $scope.userInput.startDate; 
              });
          }
          // End Date
          if (angular.isDefined($scope.userInput.endDate)) {
              searchResults = searchResults.filter(function(res){ 
              	  var testDateTime = new Date(res.testDateTime);
                  return testDateTime <= $scope.userInput.endDate; 
              });
          }
          
          // protocolName
          if ($scope.options.selectMultipleProtocols) {
              if ( Object.keys($scope.userInput.protocolNames).length <= 0) {
                console.log('No Protocol Name(s) Selected!');
                // consider notifying user...
              }
              if (angular.isDefined($scope.userInput.protocolNames)) {
                  searchResults = searchResults.filter(function(res){
                      for (var key in $scope.userInput.protocolNames) {
                          if ($scope.userInput.protocolNames.hasOwnProperty(key) && $scope.userInput.protocolNames[key]) {
                              if (res.protocolName === key) { return true; }
                          }
                      }
                      return false;
                  });
              } else {
                  // why not defined??
              }
          } else if (!$scope.options.selectMultipleProtocols) {
              if (angular.isDefined($scope.userInput.protocolName)) {
          	    searchResults = searchResults.filter(function(res) { return res.protocolName === $scope.userInput.protocolName; });
              } else {
                  console.log('No Protocol Name Selected!');
              }
          }
          
          // tabletUUID
          if ($scope.options.selectMultipleTabletUUIDs) {
              if (Object.keys($scope.userInput.tabletUUIDs).length > 0) {
                  console.log('No Tablet UUID(s) Selected!');
                  // consider notifying user...
              }
              if (angular.isDefined($scope.userInput.tabletUUIDs)) {
                  searchResults = searchResults.filter(function(res){
                      for (var key in $scope.userInput.tabletUUIDs) {
                          if ($scope.userInput.tabletUUIDs.hasOwnProperty(key) && $scope.userInput.tabletUUIDs[key]) {
                              if (res.tabletUUID === key) { return true; }
                          }
                      }
                      return false;
                  });
              } else {
                // why not defined?
              }
          } else if (!$scope.options.selectMultipleTabletUUIDs) {
              if (angular.isDefined($scope.userInput.tabletUUID)) {
          	    searchResults = searchResults.filter(function(res) { return res.tabletUUID === $scope.userInput.tabletUUID; });
              } else {
                  console.log('No Tablet UUID Selected!');
              }
          }
          
          // nResponses
          if ($scope.options.selectRangeNResponses) {
            searchResults = searchResults.filter(function(res){
                var n = res.nResponses || 0;
                console.log('nResponses: ' + n + '.  Min: ' + $scope.userInput.nResponsesMin + ', Max: ' + $scope.userInput.nResponsesMax);
                return n >= $scope.userInput.nResponsesMin && n <= $scope.userInput.nResponsesMax;
            });
          } else {
            if (Object.keys($scope.userInput.nResponses).length <= 0) {
              console.log('No nResponses(s) Selected!');
              // consider notifying user...
            }
            if (angular.isDefined($scope.userInput.nResponses)) {
                  searchResults = searchResults.filter(function(res){
                      for (var key in $scope.userInput.nResponses) {
                          if ($scope.userInput.nResponses.hasOwnProperty(key) && $scope.userInput.nResponses[key]) {
                              if (res.nResponses === parseInt(key)) { return true; }
                          }
                      }
                      return false;
                  });
            } else {
              // why not defined?!
            }
          }
          
          $scope.data.searchResults = searchResults;
      };
      
      $.ajax({
          url : "data.json",
          dataType: "json",
          success : onSuccess
      });
      
      function onSuccess(dataHeader){
          dataHeader.forEach(function(res, ind, arr){
            var d = new Date(res.testDateTime);
            arr[ind].testDateTime = d.toLocaleString();
          });
          $scope.data.header = dataHeader;
          $scope.data.categories = Object.getOwnPropertyNames(dataHeader[0]);
          var pNames = dataHeader.map(function(el){return el.protocolName});
          pNames = unique(pNames).sort();
          $scope.data.protocolNames = pNames;
          var tabletUUIDs = dataHeader.map(function(el){return el.tabletUUID});
          tabletUUIDs = unique(tabletUUIDs).sort();
          $scope.data.tabletUUIDs = tabletUUIDs;
          var nResponses = dataHeader.map(function(el){return el.nResponses});
          nResponses = unique(nResponses).sort();
          $scope.data.nResponses = nResponses;
          var nResponsesMin = Math.min.apply(Math, nResponses);
          var nResponsesMax = Math.max.apply(Math, nResponses);
          $scope.options.nResponsesSlider.floor = nResponsesMin;
          $scope.options.nResponsesSlider.ceil = nResponsesMax;
          $scope.userInput.nResponsesMin = nResponsesMin;
          $scope.userInput.nResponsesMax = nResponsesMax;
          if (!$scope.$$phase){$scope.$apply();} // the data updates after the async file load sometimes need a push to refresh
          $scope.search();
      }
      
      $scope.exportJson = function() {
      	var jsonData = JSON.stringify($scope.data.loadedData);
      	download(jsonData, 'demo.json', 'text/plain');
      };
      
      $scope.exportTxt = function() {
      	var jsonData = JSON.stringify($scope.data.loadedData);
      	download(jsonData, 'demo.txt', 'text/plain');
      };
      
      $scope.exportXLSX = function() {
          // see examples like this one for more complex excel sheet generation:
          // view-source:http://sheetjs.com/demos/writexlsx.html
          var d = $scope.data.loadedData;
          var ws = XLSX.utils.json_to_sheet(d);
          // d = [[1,2],[3,4]]; // for testing
          // var ws = XLSX.utils.aoa_to_sheet(d); // for testing (aoa = array of array)
          var wb = XLSX.utils.book_new();
          var wopts = { bookType:'xlsx', bookSST:false, type:'binary' };
          XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
          var wbout = XLSX.write(wb,wopts);
          download(s2ab(wbout), 'test.xlsx', 'application/octet-stream');
          //saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), "test.xlsx"); // only works if we load the FileSaver dependency https://github.com/eligrey/FileSaver.js
          
          function s2ab(s) {
            var buf, i;
          	if(typeof ArrayBuffer !== 'undefined') {
          		buf = new ArrayBuffer(s.length);
          		var view = new Uint8Array(buf);
          		for (i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
          		return buf;
          	} else {
          		buf = new Array(s.length);
          		for (i=0; i!=s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
          		return buf;
          	}
          }

      };
      
      function download(text, name, type) {
          var a = document.createElement("a");
          var file = new Blob([text], {type: type});
          a.href = URL.createObjectURL(file);
          a.download = name;
          a.click();
      }
      
      function unique(arr) {
          return arr.filter(function(item, pos) { return arr.indexOf(item) == pos;});
      }
      
  });