# Automated Gitlab Results Processing

Use a gitlab runner to automatically process results each time a new result is 
committed to a repo.

The python script parses all results files, and the html/js/css are used to
generate an interactive results search and export tool on the gitlab repo's
public 'pages' site.  

[More about gitlab pages](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_two.html).

## Setup:

- Create a gitlab repo for results
- Create a '.gitlab-ci.yml' in the results repo
- Copy the below code into the '.gitlab-ci.yml' file:

```yml
# Clones standard results processing code from the Creare repo into your own repo.

# Official docker image.
image: continuumio/anaconda3

pages:
  stage: deploy
  script:
  - mkdir tabsint-ci-results
  - git clone https://gitlab.com/creare-com/tabsint-ci-results.git tabsint-ci-results
  - bash -ex tabsint-ci-results/run.sh

  artifacts:
    paths:
    - public

  only:
  - master
```